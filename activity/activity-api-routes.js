import express from "express";
import cors from "cors";
import eventDao from "../dao/event-dao-mongoose.js";
import activityDao from "../dao/activity-dao-mongoose.js";
import ThisPersistentModel from "../dao/event-dao-mongoose.js";

const apiRouter = express.Router();
const apiRoute = express.Router();

var PersistentEventModel = eventDao.ThisPersistentModel;
var app = express();

apiRouter.use(
  cors({
    origin: ["http://localhost:4200"], //'*',
    methods: ["GET", "POST", "DELETE", "UPDATE", "PUT"],
  })
);

function statusCodeFromEx(ex) {
  let status = 500;
  let error = ex ? ex.error : null;
  switch (error) {
    case "BAD_REQUEST":
      status = 400;
      break;
    case "NOT_FOUND":
      status = 404;
      break;
    //...
    case "CONFLICT":
      status = 409;
      break;
    default:
      status = 500;
  }
  return status;
}


apiRouter.route("/activity-api/activities").get(async function (req, res, next) {
  try {
    let activities = await activityDao.findAll();
    res.header("Access-Control-Allow-Origin", "*");
    res.send(activities);
  } catch (ex) {
    res.status(statusCodeFromEx(ex)).send(ex);
  }

});



apiRouter.route("/activity-api/activity/:id").get(async function (req, res, next) {
    var id = req.params.id;
    try {
      let activity = await activityDao.findById(id);
      res.header("Access-Control-Allow-Origin", "*");
      res.send(activity);
    } catch (ex) {
      res.status(statusCodeFromEx(ex)).send(ex);
    }
  });

export default { apiRouter, apiRoute };
// CORS enabled with express/node-js :
