import  {MongoClient} from "mongodb"; 

// Replace the uri string with your MongoDB deployment's connection string.
const uri =
  "mongodb://localhost:27017";
const client = new MongoClient(uri);
async function run() {
  try {
    await client.connect();
   
    // database and collection code goes here
    const db = client.db("mood_db");
    const coll = db.collection("activities");

    // insert code goes here
    const docs = [

    {
    name: "Regarder un film", 
    image: "https://images.unsplash.com/photo-1584905066893-7d5c142ba4e1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8d2F0Y2hpbmclMjBtb3ZpZXxlbnwwfHwwfHw%3D&w=1000&q=80", 
    descriptionCourte : "Rien de mieux qu'un bon film quand on a pas le moral !", 
    descriptionLongue : "Le fait de voir films a beaucoup des bienfaits pour votre santé, et on ne parle pas seulement de la santé physique, mais de la santé psychologique. Quand on rit et quand on pleure en regardant un film, on trouve que ce film n’est pas seulement un groupe des images qui forment une pellicule photographique.", 
    videoType1 : "i4KI7t4Hskw&list=PL072649HMqY3F7feesZcZRj0nlV_07VK3",
    videoType2 : "rAfbgA3FO7I&list=PL072649HMqY3F7feesZcZRj0nlV_07VK3&index=65", 
    videoType3 : "dnmeEMELFM8&list=PL072649HMqY3F7feesZcZRj0nlV_07VK3&index=3",
    },

    {name: "Ecouter de la musique", 
    image: "https://afriqueitnews.com/wp-content/uploads/applications-gratuites-pour-ecouter-de-la-musique.png", 
    descriptionCourte : "La musique adoucit les mœurs, et prend soin de notre santé, comme le montrent plusieurs études.", 
    descriptionLongue : "Vous cherchez un moyen simple et rapide pour vous remonter le moral ? Des études ont prouvé que la musique est capable de stimuler l’humeur et de diminuer les risques de dépression. Elle améliore également la circulation sanguine, réduit le taux d’hormones liées au stress et soulage la douleur. Écouter de la musique avant une opération peut même améliorer les résultats post-opératoires. La musicothérapie fait d'ailleurs partie des thérapies par l'art, des psychothérapies qui utilisent la création artistique.", 
    videoType1 : "HYmawHNDFpo",
    videoType2 : "CggaaqdWSbs", 
    videoType3 : "kJQP7kiw5Fk&list=PL5_78JU9eaAlRU7K7b66gWFPoYbUqb6u9",
    },

    {name: "Faire de la méditation", 
    image: "https://medias.pourlascience.fr/api/v1/images/view/5a82a9b18fe56f0fa037c7f5/wide_1300/image.jpg", 
    descriptionCourte : "La méditation est une pratique d’entraînement de l’esprit favorisant le bien-être mental.", 
    descriptionLongue : "Méditer c’est utiliser certaines techniques de concentration et de relaxation afin de se concentrer sur soi et ainsi, faire taire son brouhaha intérieur. C’est une parenthèse dans notre quotidien stressant bruyant au rythme infernal et trop rapide : c’est pouvoir se poser, s’arrêter et observer ce qui se passe en nous…", 
    videoType1 : "b5KtVjy2byI",
    videoType2 : "bad5rdzhNEk", 
    videoType3 : "snlZ9WTJiI4",
    },

    {name: "Faire du Yoga - niveau 1", 
    image: "https://www.helsana.ch/dam/bilder/blog/yoga-fuer-anfaenger.hlsimg.1_1.w1024.jpg", 
    descriptionCourte : "Le Yoga est la pratique d’un ensemble de postures et d’exercices de respiration qui vise à apporter un bien être physique et mental.", 
    descriptionLongue : "Le yoga Ashtanga est une forme de Hatha yoga au sein duquel les postures sont accompagnées d’étirements permettant de donner de l’énergie, de la force au corps ; et de contractions (Bandas) visant à accumuler le souffle vital (prana) dans les parties profondes des tissus du corps via une synchronisation des mouvement avec la respiration (vinyasa). ", 
    videoType1 : "Z8In0I1WHFs",
    videoType2 : "WK92nupSKkc", 
    videoType3 : "jwPFXr5dXyc",
    },

    {name: "Faire du Yoga - niveau 2", 
    image: "https://fac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Ffac.2F2019.2F04.2F25.2F2be3f767-c8e9-4440-9e60-6a163fb55f38.2Ejpeg/1200x900/quality/80/crop-from/center/focus-point/1025%2C760/yoga-pour-debutant-les-positions-a-connaitre-pour-reussir-son-premier-cours.jpeg", 
    descriptionCourte : "Le Yoga est la pratique d’un ensemble de postures et d’exercices de respiration qui vise à apporter un bien être physique et mental.", 
    descriptionLongue : "Le Vinyasa yoga est une pratique dynamique et sportive du yoga, dans laquelle chaque mouvement est initié par la respiration. En plus d’être une activité physique complète, il confère de nombreux bienfaits sur le corps, la santé et l’esprit. Le principe de ce yoga est de placer les différentes postures sur la respiration de manière synchronisée, harmonique et dynamique. Les postures s’enchainent dans un flux naturel, nommé « Flow », alliant équilibre, renforcement, respiration et lâcher-prise.", 
    videoType1 : "VEFRtZX-f6I",
    videoType2 : "cwus84O-mHE&t=4s", 
    videoType3 : "04_17KIf9ME",
    },

    {name: "Faire du Yoga - niveau 3", 
    image: "https://static.nike.com/a/images/f_auto,cs_srgb/w_1920,c_limit/43f01829-d882-4999-bd6c-0e958191c670/les-trois-meilleures-postures-de-yoga-pour-gagner-en-force-selon-les-experts.jpg", 
    descriptionCourte : "Le Yoga est la pratique d’un ensemble de postures et d’exercices de respiration qui vise à apporter un bien être physique et mental.", 
    descriptionLongue : "Le Hatha-Yoga ou Hathayoga est un terme sanskrit. Ce type de yoga, par la pratique précise et rythmée de postures, permet au pratiquant une maîtrise du corps et des sens. Il trouve son origine dans des textes anciens, les yoga sutras de Patanjali.", 
    videoType1 : "l3oAT3X1eL0",
    videoType2 : "fh6S3yQ8360", 
    videoType3 : "daDoTMyoLDg",
    },

    {name: "Danser la Zumba", 
    image: "https://www.salsadanse.com/wp-content/uploads/2020/07/cours-de-zumba-latin-training-paris-4.jpg", 
    descriptionCourte : "La Zumba est une méthode de fitness combinant exercices physiques, danses et musiques latines.", 
    descriptionLongue : "La Zumba est un programme d'entraînement physique complet, alliant tous les éléments de la remise en forme : cardio et préparation musculaire, équilibre et flexibilité. Les chorégraphies s'inspirent principalement des danses latines (salsa, merengue, cumbia, reggaeton, soca, samba, tango, flamenco…), mais aussi de styles variés comme la danse du ventre ou la Quebradita. ", 
    videoType1 : "8DZktowZo_k",
    videoType2 : "EMAyb2E6gNw", 
    videoType3 : "SChZ42s-aNE",
    },

    {name: "Danser la Bokwa", 
    image: "https://cdn-s-www.bienpublic.com/images/54B01E74-7C1A-4DA8-8B43-CC20729445CE/NW_raw/un-cours-de-bokwa-permet-de-depenser-jusqu-a-1-200-calories-bokwa-1503238177.jpg", 
    descriptionCourte : "Bokwa, est une danse d'inspiration sud africaine c’est la contraction des mots “boxe” et “kwaito”.", 
    descriptionLongue : "Plus simple à pratiquer que la très populaire zumba, la bokwa, inventée par un danseur d'origine sud-africaine à Los Angeles, rime avec fun et défoulement. Inutile de savoir danser pour pratiquer ce mélange de danse, cardio et musculation, avec des mouvements empruntés à la boxe. En cette rentrée, l'activité sportive est l'option idéale pour conserver la bonne humeur des vacances. Sur des musiques ultra rythmées, la chorégraphie de la bokwa, inspirée de la danse des ghettos sud-africains, consiste à tracer des chiffres et des lettres avec les pas sur le sol, la plupart du temps en rebondissant.", 
    videoType1 : "E5YAdUyjA2M",
    videoType2 : "xEvFcPLOwwE", 
    videoType3 : "6T21dasU1gw",
    },

    {name: "Afrofitness", 
    image: "https://thedancehall.sn/wp-content/uploads/2017/02/8-min.png", 
    descriptionCourte : "L'Afro Fitness est une danse sportive sur des rythmiques Afro.", 
    descriptionLongue : "Il permet de travailler, de manière agréable, ses fesses, ses cuisses et son cardio. La danse parfaite pour raffermir sont corps et brûler des calories tout en s'amusant !", 
    videoType1 : "3_bCT-GUMBY",
    videoType2 : "YDes3xOd9LQ", 
    videoType3 : "Ev6zzaGkwOY",
    },

    {name: "Origami", 
    image: "https://images.unsplash.com/photo-1563260797-cb5cd70254c8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8b3JpZ2FtaXxlbnwwfHwwfHw%3D&w=1000&q=80", 
    descriptionCourte : "L'Origami est l’art du pliage en papier. Un art ancestral, aussi délicat qu’économique, apprécié autant par les enfants que les adultes", 
    descriptionLongue : "C’est un art qui permet de réaliser, à l'aide d'une simple feuille de papier, généralement de forme carrée, une succession de pliages ingénieux, pour arriver, sans opérer de découpage ou de collage, à la représentation figurative ou non, de toutes sortes de modèles.", 
    videoType1 : "ujE-gSZdOpE",
    videoType2 : "25qjLa9ZzBM", 
    videoType3 : "vef3JKIsDgI",
    },

    {name: "Méditation", 
    image: "https://p4.wallpaperbetter.com/wallpaper/658/207/830/beach-meditation-sunlight-water-wallpaper-preview.jpg", 
    descriptionCourte : "La méditation est une pratique d’entraînement de l’esprit favorisant le bien-être mental", 
    descriptionLongue : "Méditer c’est utiliser certaines techniques de concentration et de relaxation afin de se concentrer sur soi et ainsi, faire taire son brouhaha intérieur. C’est une parenthèse dans notre quotidien stressant bruyant au rythme infernal et trop rapide : c’est pouvoir se poser, s’arrêter et observer ce qui se passe en nous… La pratique de la méditation consiste avant tout à s'entraîner à maintenir son attention et à empêcher son esprit de se laisser emporter par les pensées qui surgissent sans arrêt. Cela dit, il ne s'agit surtout pas d'une activité guerrière où il faut se battre contre les pensées. On a plutôt recours à la « volonté douce ». C’est une activité de lâcher-prise où l'on accepte que les pensées défilent, comme des nuages ou les chevaux d’un carrousel, sans pour autant se laisser captiver par elles", 
    videoType1 : "b5KtVjy2byI",
    videoType2 : "bad5rdzhNEk", 
    videoType3 : "snlZ9WTJiI4",
    },

    {name: "Footing", 
    image: "https://www.ownsport.fr/blog/wp-content/uploads/2020/07/footing-1-1024x654.jpg", 
    descriptionCourte : "Le footing, consiste à courir à pied et à faible intensité sur une certaine distance.", 
    descriptionLongue : "C’est une activité physique au cours de laquelle on peut échanger sans s’essouffler tout en travaillant son endurance.On en distingue plusieurs types : le footing lent, le footing à allure moyenne, le footing rapide, le footing à accélération progressive et le footing actif.Le footing est aussi pratiqué pour développer les muscles de la cuisse et ceux de la jambe. S’il est pratiqué très souvent avant les entraînements, il est utilisé également pour récupérer après une compétition comme un marathon.", 
    videoType1 : "hijkKi4g_q8",
    videoType2 : "pP7TCO3kqoM", 
    videoType3 : "A0yAN49Dngs",
    },

    {name: "Etirements", 
    image: "https://www.medisite.fr/files/styles/pano_xxxl/public/images/article/6/8/9/5593986/vignette-focus.jpg?itok=PmuZk7sw", 
    descriptionCourte : "Tout le monde connait les étirements mais ils sont trop souvent délaissés malgré leurs effets bénéfiques sur la récupération, la posture et la prévention des blessures.", 
    descriptionLongue : "Concrètement, s'étirer, c'est rendre les muscles plus élastiques et donc entretenir la mobilité du corps. Un corps mobile est un corps en meilleure santé et cela participe de la prévention des blessures. Si on ne s'étire pas régulièrement, nos muscles perdront en souplesse en vieillissant, ce qui peut occasionner pas mal de désagréments qui varient d'un individu à l'autre (comme la lombalgie, l'arthrose, la cervicalgie...), poursuit M. Almoyner. C'est aussi ce qui nous fait parfois nous sentir rouillées, d'où l'intérêt de prendre soin de notre souplesse.", 
    videoType1 : "xvrWZk6ZekQ",
    videoType2 : "G8FPqq-hFXI", 
    videoType3 : "fbOlMaiakM4",
    },

    {name: "Gym Suédoise", 
    image: "https://fac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2FFAC.2Fvar.2Ffemmeactuelle.2Fstorage.2Fimages.2Fbien-etre.2Fsport-fitness.2Fbonnes-raisons-de-se-mettre-a-la-gym-suedoise-48144.2F14891111-1-fre-FR.2F3-bonnes-raisons-de-se-mettre-a-la-gym-suedoise.2Ejpg/1200x1200/quality/80/crop-from/center/3-bonnes-raisons-de-se-mettre-a-la-gym-suedoise.jpeg", 
    descriptionCourte : "Faire une activité bonne pour le corps et le mental, le tout en musique : ce sont les promesses de la Gym Suédoise.", 
    descriptionLongue : "Les différents exercices proposés dans les cours de gym suédoise font travailler l'ensemble du corps grâce à des mouvements simples, mais toujours très toniques.", 
    videoType1 : "0NFa33_HBi8",
    videoType2 : "JGkhHEKywt4", 
    videoType3 : "sZor_7AlIM8",
    },

    {name: "Vélo", 
    image: "https://www.origine-cycles.com/images/categorieFamille/1_1_velos-de-route.jpg", 
    descriptionCourte : "Accessible à tous, quel que soit son âge et sa condition physique, le vélo est idéal pour reprendre une activité physique.", 
    descriptionLongue : "Outre le fait de travailler son équilibre, le vélo améliore l’endurance. Cette activité permet aussi de se muscler – en particulier au niveau des jambes – et elle est particulièrement recommandée pour les personnes en surpoids, car elle est sans impact pour les articulations.", 
    videoType1 : "e-NCf_hPgqc",
    videoType2 : "x2RIPNeBL3g", 
    videoType3 : "zYKWFT1vgzk",
    },

    {name: "Lire ou écouter un livre", 
    image: "http://www.mangoandsalt.com/wp-content/uploads/2017/01/comment-lire-plus.jpg", 
    descriptionCourte : "La lecture peut être définie comme une activité psychosensorielle qui vise à donner un sens à des signes graphiques recueillis par la vision et qui implique à la fois des traitements perceptifs et cognitifs", 
    descriptionLongue : "Lire se fait traditionnellement en silence. C’est une parenthèse dans notre quotidien, un temps de relaxation. Le corps se met au repos et notre attention se fixe sur une histoire qui nous propulse dans l’imaginaire, s’il s’agit d’un roman, dans un ailleurs. La lecture en plus de nous détendre booste ainsi notre créativité en nous faisant traverser une histoire aux rebondissements inattendus, en nous faisant parcourir l’inconnu. C’est un divertissement apaisant.", 
    videoType1 : "2QJfA8CPIcE&list=PLFK7qxfVITr04xEKgQXv66FR0AejevxNy&index=47",
    videoType2 : "LlSSXHv4190&list=PLFK7qxfVITr04xEKgQXv66FR0AejevxNy&index=42", 
    videoType3 : "0yc6WYyFx_k&list=PLFK7qxfVITr04xEKgQXv66FR0AejevxNy&index=39",
    },


];
const result = await coll.insertMany(docs);


    // display the results of your operation
    console.log(result.insertedIds);
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);