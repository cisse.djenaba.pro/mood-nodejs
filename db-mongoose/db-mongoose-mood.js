import mongoose from "mongoose";

var mongoDbUrl =
  process.env.MONGODB_URL ||
  "mongodb://localhost:27017/"; //by default

console.log("mongoDbUrl=" + mongoDbUrl);
mongoose.connect(mongoDbUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  dbName: "mood_db",
});
var moodDb = mongoose.connection;

moodDb.on("error", function () {
  console.log("mongoDb connection error = " + " for dbUrl=" + mongoDbUrl);
});

moodDb.once("open", function () {
  // we're connected!
  console.log("Connected correctly to mongodb database");
});

export default { moodDb };
