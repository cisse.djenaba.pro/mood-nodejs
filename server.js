import express from "express";
import mongoose from "mongoose";
import dbMongoose from "./db-mongoose/db-mongoose.js";

import weatherApiRoutes from "./routes/weather-api-routes.js";
import eventApiRoutes from "./routes/event-api-routes.js";
import activityApiRoutes from "./routes/activity-api-routes.js";


import { dirname } from "path";
import { fileURLToPath } from "url";



const __dirname = dirname(fileURLToPath(import.meta.url));
var app = express();

mongoose.connection = dbMongoose.db;


var jsonParser = express.json({ extended: true });
app.use(jsonParser);

app.use(weatherApiRoutes.apiRouter);
app.use(weatherApiRoutes.apiRoute);
app.use(eventApiRoutes.apiRouter); 
app.use(activityApiRoutes.apiRouter); 


app.listen(8090, function () {
  console.log("http://localhost:8090");
});
