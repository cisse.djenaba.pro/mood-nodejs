import mongoose from "mongoose"; 


module.exports = mongoose => {
  const Mood = mongoose.model(
    "Mood",
    mongoose.Schema(
      {
        mood: String,
      },
      { timestamps: true }
    )
  );
  return Mood;
};

export { mongoose };
export { default } ; 