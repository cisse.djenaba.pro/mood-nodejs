import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import weatherDao from "../dao/weather-dao-mongoose.js";
import moodDao from "../dao/mood-dao-mongoose.js"; 

import axios from 'axios'


const app = express();
const router = express.Router();

app.use(cors());
app.use(bodyParser.json());



function statusCodeFromEx(ex) {
  let status = 500;
  let error = ex ? ex.error : null;
  switch (error) {
    case "BAD_REQUEST":
      status = 400;
      break;
    case "NOT_FOUND":
      status = 404;
      break;
    //...
    case "CONFLICT":
      status = 409;
      break;
    default:
      status = 500;
  }
  return status;
}


router.route('/').get(function (req,res) {
    res.send("express works");
});

router.route('/registerMood').post((req, res, next) => {
    console.log(req.body); 
    var moodData = req.body; 

    try{
		let savedMood = moodDao.save(moodData);
		res.send(savedMood);
    } catch(ex){
	    res.status(statusCodeFromEx(ex)).send(ex);
    }
        
});



app.use('/', router);

export default { router }; 
