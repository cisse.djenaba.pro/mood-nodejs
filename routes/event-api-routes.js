import express from "express";
import cors from "cors";
import eventDao from "../eventBot/event-dao-mongoose.js";

const apiRouter = express.Router();
const apiRoute = express.Router();

apiRouter.use(
  cors({
    origin: ["http://localhost:4200"], //'*',
    methods: ["GET", "POST", "DELETE", "UPDATE", "PUT"],
  })
);

function statusCodeFromEx(ex) {
  let status = 500;
  let error = ex ? ex.error : null;
  switch (error) {
    case "BAD_REQUEST":
      status = 400;
      break;
    case "NOT_FOUND":
      status = 404;
      break;
    //...
    case "CONFLICT":
      status = 409;
      break;
    default:
      status = 500;
  }
  return status;
}

apiRouter.route("/event-api/event/:zip").get(async function (req, res, next) {
  var zip = req.params.zip;
  try {
    let event = await eventDao.findById(zip);
    res.header("Access-Control-Allow-Origin", "*");
    res.send(event);
  } catch (ex) {
    res.status(statusCodeFromEx(ex)).send(ex);
  }
});

apiRouter.route("/event-api/events").get(async function (req, res, next) {
  try {
    let events = await eventDao.findAll();
    res.header("Access-Control-Allow-Origin", "*");
    res.send(events);
  } catch (ex) {
    res.status(statusCodeFromEx(ex)).send(ex);
  }
});

export default { apiRouter, apiRoute };
// CORS enabled with express/node-js :
